import sys

from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
import csv


browser = webdriver.Chrome(ChromeDriverManager().install())
browser.get('http://boozle.com.au/browse/s/t:Beer,+c:Ale')

try:
    with open('products.csv', 'w', newline='') as csvfile:

        filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Name', 'Image', 'Stars', 'Reviews Count', 'Low Price', 'High Price'])

        product_list = WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'li.price-vo')))

        for product in product_list:
            name = product.find_element_by_css_selector("div.product-name>h3").text
            image = product.find_element_by_css_selector("div.logo").get_attribute("style").replace(';', '')

            try:
                stars = product.find_element_by_css_selector("span[class='stars-m tooltip']").get_attribute("title")
            except NoSuchElementException:
                stars = 'n/a'

            try:
                reviews_count = product.find_element_by_css_selector("span[itemprop='ratingCount']").text
            except NoSuchElementException:
                reviews_count = 0

            try:
                low_price = product.find_element_by_css_selector("span[itemprop='lowPrice']").text
            except NoSuchElementException:
                low_price = 'n/a'

            try:
                high_price = product.find_element_by_css_selector("span[itemprop='highPrice']").text
            except NoSuchElementException:
                high_price = 'n/a'

            filewriter.writerow([name, image, stars, reviews_count, low_price, high_price])

    browser.quit()
except TimeoutException:
    print("Loading took too much time!")

sys.exit()